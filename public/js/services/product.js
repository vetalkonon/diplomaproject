/*
// js/services/todos.js
angular.module('todoService', [])

    // super simple service
    // each function returns a promise object 
    .factory('Todos', function($http) {
        return {
            get : function() {
                return $http.get('/api/todos');
            },
            create : function(todoData) {
                return $http.post('/api/todos', todoData);
            },
            delete : function(id) {
                return $http.delete('/api/todos/' + id);
            }
        }
    });
*/

// js/services/todos.js
angular.module('productService', [])

    // super simple service
    // each function returns a promise object 
    .factory('products', function($http) {
        return {
            get : function() {
                return $http.get('/api/products');
            },
            create : function(data) {
                return $http.post('/api/products', todoData);
            },
            delete : function(id) {
                return $http.delete('/api/products/' + id);
            }
        }
    });
