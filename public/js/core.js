// public/core.js
var store = angular.module('store', []);

function StoreController($scope, $http) {
    $scope.pageData = {};

    // when landing on the page, get all todos and show them
    $http.get('/api/products')
        .success(function (data) {
            $scope.products = data;
            console.log(data);
        })
        .error(function (data) {
            console.log('Error: ' + data);
        });

    // when submitting the add form, send the text to the node API
    $scope.createTodo = function () {
        $http.post('/api/products', $scope.pageData)
            .success(function (data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.products = data;
                conssole.log(data);
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });
    };

    // delete a todo after checking it
    $scope.deleteTodo = function (id) {
        $http.delete('/api/products/' + id)
            .success(function (data) {
                $scope.products = data;
                console.log(data);
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });
    };

}
