/*
// app/models/todo.js

    // load mongoose since we need it to define a model
    var mongoose = require('mongoose');

    module.exports = mongoose.model('Todo', {
        text : String,
        done : Boolean
    });
*/
// app/models/todo.js

// load mongoose since we need it to define a model
var mongoose = require('mongoose');
var imgObj = require('img');
var reviewObj = require('review');

module.exports = img.model('imgObj', {
    full: String,
    thumb: String
});

module.exports = review.model('reviewObj', {
    stars: Number,
    body: String,
    author: String
});

module.exports = mongoose.model('Product', {
    name: String,
    price: Number,
    description: String,
    canPurchase: Boolean,
    bought: Number,
    soldOut: Boolean,
    images: Array[imgObj],
    reviews: Array[reviewObj]
});
