/* // app/routes.js ======================================================================
 // load the todo model
    var Todo = require('./models/todo');

    // expose the routes to our app with module.exports
    module.exports = function(app) {
    // api ---------------------------------------------------------------------
    // get all todos
    app.get('/api/todos', function(req, res) {

        // use mongoose to get all todos in the database
        Todo.find(function(err, todos) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)

            res.json(todos); // return all todos in JSON format
        });
    });

    // create todo and send back all todos after creation
    app.post('/api/todos', function(req, res) {

        // create a todo, information comes from AJAX request from Angular
        Todo.create({
            text : req.body.text,
            done : false
        }, function(err, todo) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Todo.find(function(err, todos) {
                if (err)
                    res.send(err)
                res.json(todos);
            });
        });

    });

    // delete a todo
    app.delete('/api/todos/:todo_id', function(req, res) {
        Todo.remove({
            _id : req.params.todo_id
        }, function(err, todo) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Todo.find(function(err, todos) {
                if (err)
                    res.send(err)
                res.json(todos);
            });
        });
    });

  // application -------------------------------------------------------------
    app.get('*', function(req, res) {
        res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

};*/
// app/routes.js ======================================================================
// load the todo model
var Todo = require('./models/product');

// expose the routes to our app with module.exports
module.exports = function (app) {
    // api ---------------------------------------------------------------------
    // get all todos
    app.get('/api/products', function (req, res) {

        // use mongoose to get all todos in the database
        Product.find(function (err, products) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)

            res.json(product); // return all todos in JSON format
        });
    });

    // create todo and send back all todos after creation
    app.post('/api/product', function (req, res) {

        // create a todo, information comes from AJAX request from Angular
        Product.create({
            name: String,
            price: Number,
            description: String,
            canPurchase: Boolean,
            bought: Number,
            soldOut: Boolean,
            images: Array[imgObj],
            reviews: Array[reviewObj],
            text: req.body.text,
            done: false
        }, function (err, product) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Product.find(function (err, products) {
                if (err)
                    res.send(err)
                res.json(products);
            });
        });

    });

    // delete a todo
    app.delete('/api/products/:product_id', function (req, res) {
        Product.remove({
            _id: req.params.product_id
        }, function (err, todo) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Product.find(function (err, todos) {
                if (err)
                    res.send(err)
                res.json(products);
            });
        });
    });

    // application -------------------------------------------------------------
    app.get('*', function (req, res) {
        res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

};
